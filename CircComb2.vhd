library ieee;
use ieee.std_logic_1164.all;
 
entity CircComb2 is
  port (
    q  : in std_logic_vector(1 downto 0);
    --
    F : out std_logic_vector(25 downto 20)
    );
end CircComb2;
 
 
architecture rtl of CircComb2 is


begin

	with q select F <=
	
	"000000" when "00",
		
	"001001" when "01",
	
	"011110" when "10",
	
	"111111" when "11";
		 
 
end rtl;