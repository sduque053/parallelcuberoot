library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lvl9 is
	port(
	
		D : in std_logic_vector(25 downto 0);
		
		P : in std_logic_vector(19 downto 1);
		
		R : out std_logic_vector(25 downto 0);
		
		Q : out std_logic
		
		);
end lvl9;

architecture RTL of lvl9 is

	component BC
		port(
	
			A, B, Cin, MuxI	: in std_logic;
	
			O, Cout, MuxO		: out std_logic
	
		);
	end component;
	
	component BCS
		port(
	
			A, MuxI				: in std_logic;
	
			O, Cout, MuxO		: out std_logic
	
		);
	end component;
	
	signal CO : std_logic_vector(25 downto 0);
	signal CI : std_logic_vector(25 downto 1);
	signal MO : std_logic_vector(25 downto 0);
	signal MI : std_logic_vector(25 downto 0);
	signal F	 : std_logic_vector(25 downto 1);
	
	
	begin
	
		G1: for I in 0 to 25 generate
			
			FC: if I=0 generate
				BC1: BCS port map(
					D(I), MI(I), R(I), CO(I), MO(I)
				);
			end generate FC;
			
			OC: if i>0 generate
				BC2: BC port map(
					D(I), F(I), CI(I), MI(I), R(I), CO(I), MO(I)
				);
			end generate OC;
			
		end generate G1;
		
		F(25 downto 20)<="000000";
		F(19 downto 1) <= P(19 downto 1);
		
		MI(25) <= CO(25);
		Q <= MO(0);
		
		G2: for I in 0 to 24 generate
			CI(I+1) <= CO(I);
			MI(I) <= MO(I+1);
		end generate G2;
		

end RTL;
		