library ieee;
use ieee.std_logic_1164.all;
 
entity CircComb1 is
  port (
    q  : in std_logic;
    --
    F : out std_logic_vector(13 downto 10)
    );
end CircComb1;
 
 
architecture rtl of CircComb1 is


begin

	with q select F <=
	
	"1001" when '1',
	"0000" when '0';
 
 
end rtl;