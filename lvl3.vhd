library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lvl3 is
	port(
	
		D : in std_logic_vector(7 downto 0);
		
		P : in std_logic_vector(6 downto 1);
		
		R : out std_logic_vector(7 downto 0);
		
		Q : out std_logic
		
		);
end lvl3;

architecture RTL of lvl3 is

	component BC
		port(
	
			A, B, Cin, MuxI	: in std_logic;
	
			O, Cout, MuxO		: out std_logic
	
		);
	end component;
	
	component BCS
		port(
	
			A, MuxI				: in std_logic;
	
			O, Cout, MuxO		: out std_logic
	
		);
	end component;
	
	signal CO : std_logic_vector(7 downto 0);
	signal CI : std_logic_vector(7 downto 1);
	signal MO : std_logic_vector(7 downto 0);
	signal MI : std_logic_vector(7 downto 0);
	signal F	 : std_logic_vector(7 downto 1);
	
	
	begin
	
		G1: for I in 0 to 7 generate
			
			FC: if I=0 generate
				BC1: BCS port map(
					D(I), MI(I), R(I), CO(I), MO(I)
				);
			end generate FC;
			
			OC: if i>0 generate
				BC2: BC port map(
					D(I), F(I), CI(I), MI(I), R(I), CO(I), MO(I)
				);
			end generate OC;
			
		end generate G1;
		
		F(7)<='0';
		F(6 downto 1) <= P(6 downto 1);
		
		MI(7) <= CO(7);
		Q <= MO(0);
		
		G2: for I in 0 to 6 generate
			CI(I+1) <= CO(I);
			MI(I) <= MO(I+1);
		end generate G2;
		

end RTL;
		