-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"

-- DATE "01/15/2020 21:33:54"

-- 
-- Device: Altera 5CGXFC7C7F23C8 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	BCell IS
    PORT (
	Q : OUT std_logic_vector(2 DOWNTO 0);
	D : IN std_logic_vector(7 DOWNTO 0);
	R : OUT std_logic_vector(7 DOWNTO 0)
	);
END BCell;

-- Design Ports Information
-- Q[2]	=>  Location: PIN_M22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[1]	=>  Location: PIN_N21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[0]	=>  Location: PIN_M21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[7]	=>  Location: PIN_L22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[6]	=>  Location: PIN_K22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[5]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[4]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[3]	=>  Location: PIN_L17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[2]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[1]	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[0]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[7]	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[6]	=>  Location: PIN_N16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[5]	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[3]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[4]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[0]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[1]	=>  Location: PIN_K17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D[2]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF BCell IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_Q : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_R : std_logic_vector(7 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \D[6]~input_o\ : std_logic;
SIGNAL \D[7]~input_o\ : std_logic;
SIGNAL \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ : std_logic;
SIGNAL \D[5]~input_o\ : std_logic;
SIGNAL \D[3]~input_o\ : std_logic;
SIGNAL \D[4]~input_o\ : std_logic;
SIGNAL \inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ : std_logic;
SIGNAL \inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\ : std_logic;
SIGNAL \inst1|G1:2:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ : std_logic;
SIGNAL \D[0]~input_o\ : std_logic;
SIGNAL \D[1]~input_o\ : std_logic;
SIGNAL \D[2]~input_o\ : std_logic;
SIGNAL \inst2|G1:3:OC:BC2|FA|o_carry~combout\ : std_logic;
SIGNAL \inst2|G1:7:OC:BC2|FA|o_carry~combout\ : std_logic;
SIGNAL \inst2|G1:2:OC:BC2|FA|o_carry~combout\ : std_logic;
SIGNAL \inst2|G1:7:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:6:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:5:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:4:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:3:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:2:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:1:OC:BC2|O~0_combout\ : std_logic;
SIGNAL \inst2|G1:0:FC:BC1|O~combout\ : std_logic;
SIGNAL \inst1|G1:2:OC:BC2|ALT_INV_O~0_combout\ : std_logic;
SIGNAL \inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~0_combout\ : std_logic;
SIGNAL \inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~combout\ : std_logic;
SIGNAL \inst2|G1:2:OC:BC2|FA|ALT_INV_o_carry~combout\ : std_logic;
SIGNAL \ALT_INV_D[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_D[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_D[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_D[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_D[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_D[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_D[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_D[2]~input_o\ : std_logic;
SIGNAL \inst2|G1:4:OC:BC2|FA|ALT_INV_w_WIRE_1~combout\ : std_logic;
SIGNAL \inst2|G1:3:OC:BC2|FA|ALT_INV_o_carry~combout\ : std_logic;
SIGNAL \inst1|G1:4:OC:BC2|FA|ALT_INV_o_carry~0_combout\ : std_logic;
SIGNAL \inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\ : std_logic;

BEGIN

Q <= ww_Q;
ww_D <= D;
R <= ww_R;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\inst1|G1:2:OC:BC2|ALT_INV_O~0_combout\ <= NOT \inst1|G1:2:OC:BC2|O~0_combout\;
\inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~0_combout\ <= NOT \inst2|G1:7:OC:BC2|FA|o_carry~0_combout\;
\inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~combout\ <= NOT \inst2|G1:7:OC:BC2|FA|o_carry~combout\;
\inst2|G1:2:OC:BC2|FA|ALT_INV_o_carry~combout\ <= NOT \inst2|G1:2:OC:BC2|FA|o_carry~combout\;
\ALT_INV_D[7]~input_o\ <= NOT \D[7]~input_o\;
\ALT_INV_D[6]~input_o\ <= NOT \D[6]~input_o\;
\ALT_INV_D[3]~input_o\ <= NOT \D[3]~input_o\;
\ALT_INV_D[5]~input_o\ <= NOT \D[5]~input_o\;
\ALT_INV_D[4]~input_o\ <= NOT \D[4]~input_o\;
\ALT_INV_D[0]~input_o\ <= NOT \D[0]~input_o\;
\ALT_INV_D[1]~input_o\ <= NOT \D[1]~input_o\;
\ALT_INV_D[2]~input_o\ <= NOT \D[2]~input_o\;
\inst2|G1:4:OC:BC2|FA|ALT_INV_w_WIRE_1~combout\ <= NOT \inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\;
\inst2|G1:3:OC:BC2|FA|ALT_INV_o_carry~combout\ <= NOT \inst2|G1:3:OC:BC2|FA|o_carry~combout\;
\inst1|G1:4:OC:BC2|FA|ALT_INV_o_carry~0_combout\ <= NOT \inst1|G1:4:OC:BC2|FA|o_carry~0_combout\;
\inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\ <= NOT \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\;

-- Location: IOOBUF_X89_Y36_N39
\Q[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\,
	devoe => ww_devoe,
	o => ww_Q(2));

-- Location: IOOBUF_X89_Y35_N96
\Q[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst1|G1:4:OC:BC2|FA|o_carry~0_combout\,
	devoe => ww_devoe,
	o => ww_Q(1));

-- Location: IOOBUF_X89_Y37_N56
\Q[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:7:OC:BC2|FA|o_carry~combout\,
	devoe => ww_devoe,
	o => ww_Q(0));

-- Location: IOOBUF_X89_Y36_N56
\R[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:7:OC:BC2|O~0_combout\,
	devoe => ww_devoe,
	o => ww_R(7));

-- Location: IOOBUF_X89_Y38_N56
\R[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:6:OC:BC2|O~0_combout\,
	devoe => ww_devoe,
	o => ww_R(6));

-- Location: IOOBUF_X89_Y8_N5
\R[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:5:OC:BC2|O~0_combout\,
	devoe => ww_devoe,
	o => ww_R(5));

-- Location: IOOBUF_X89_Y8_N22
\R[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:4:OC:BC2|O~0_combout\,
	devoe => ww_devoe,
	o => ww_R(4));

-- Location: IOOBUF_X89_Y37_N22
\R[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:3:OC:BC2|O~0_combout\,
	devoe => ww_devoe,
	o => ww_R(3));

-- Location: IOOBUF_X89_Y36_N5
\R[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:2:OC:BC2|O~0_combout\,
	devoe => ww_devoe,
	o => ww_R(2));

-- Location: IOOBUF_X89_Y35_N62
\R[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:1:OC:BC2|O~0_combout\,
	devoe => ww_devoe,
	o => ww_R(1));

-- Location: IOOBUF_X89_Y38_N39
\R[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|G1:0:FC:BC1|O~combout\,
	devoe => ww_devoe,
	o => ww_R(0));

-- Location: IOIBUF_X89_Y35_N44
\D[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(6),
	o => \D[6]~input_o\);

-- Location: IOIBUF_X89_Y38_N4
\D[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(7),
	o => \D[7]~input_o\);

-- Location: LABCELL_X88_Y36_N30
\inst|G1:1:OC:BC2|FA|w_WIRE_2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ = (\D[6]~input_o\ & \D[7]~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010001000100010001000100010001000100010001000100010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[6]~input_o\,
	datab => \ALT_INV_D[7]~input_o\,
	combout => \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\);

-- Location: IOIBUF_X89_Y35_N78
\D[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(5),
	o => \D[5]~input_o\);

-- Location: IOIBUF_X89_Y9_N55
\D[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(3),
	o => \D[3]~input_o\);

-- Location: IOIBUF_X89_Y38_N21
\D[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(4),
	o => \D[4]~input_o\);

-- Location: LABCELL_X88_Y36_N6
\inst1|G1:4:OC:BC2|FA|o_carry~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ = ( \D[3]~input_o\ & ( \D[4]~input_o\ & ( (!\D[7]~input_o\) # (!\D[6]~input_o\) ) ) ) # ( !\D[3]~input_o\ & ( \D[4]~input_o\ & ( (!\D[7]~input_o\) # (!\D[6]~input_o\) ) ) ) # ( \D[3]~input_o\ & ( !\D[4]~input_o\ & 
-- ( (!\D[7]~input_o\) # (!\D[6]~input_o\) ) ) ) # ( !\D[3]~input_o\ & ( !\D[4]~input_o\ & ( (!\D[7]~input_o\ & ((\D[6]~input_o\) # (\D[5]~input_o\))) # (\D[7]~input_o\ & ((!\D[6]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111110001111100111111001111110011111100111111001111110011111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[5]~input_o\,
	datab => \ALT_INV_D[7]~input_o\,
	datac => \ALT_INV_D[6]~input_o\,
	datae => \ALT_INV_D[3]~input_o\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst1|G1:4:OC:BC2|FA|o_carry~0_combout\);

-- Location: LABCELL_X88_Y36_N33
\inst2|G1:4:OC:BC2|FA|w_WIRE_1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\ = ( \D[4]~input_o\ & ( (!\D[3]~input_o\ & ((!\D[6]~input_o\) # (!\D[7]~input_o\))) ) ) # ( !\D[4]~input_o\ & ( ((\D[6]~input_o\ & \D[7]~input_o\)) # (\D[3]~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001111100011111000111110001111111100000111000001110000011100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[6]~input_o\,
	datab => \ALT_INV_D[7]~input_o\,
	datac => \ALT_INV_D[3]~input_o\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\);

-- Location: LABCELL_X88_Y36_N48
\inst1|G1:2:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst1|G1:2:OC:BC2|O~0_combout\ = ( \D[4]~input_o\ & ( \D[5]~input_o\ ) ) # ( !\D[4]~input_o\ & ( (!\D[5]~input_o\ & (!\D[3]~input_o\ & (!\D[7]~input_o\ $ (!\D[6]~input_o\)))) # (\D[5]~input_o\ & (((\D[7]~input_o\ & \D[6]~input_o\)) # (\D[3]~input_o\))) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010100101010101001010010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[5]~input_o\,
	datab => \ALT_INV_D[7]~input_o\,
	datac => \ALT_INV_D[6]~input_o\,
	datad => \ALT_INV_D[3]~input_o\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst1|G1:2:OC:BC2|O~0_combout\);

-- Location: LABCELL_X88_Y36_N51
\inst2|G1:7:OC:BC2|FA|o_carry~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ = ( \D[4]~input_o\ & ( !\D[7]~input_o\ $ (!\D[6]~input_o\) ) ) # ( !\D[4]~input_o\ & ( (!\D[7]~input_o\ & (\D[6]~input_o\ & ((\D[3]~input_o\) # (\D[5]~input_o\)))) # (\D[7]~input_o\ & (((!\D[6]~input_o\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011010000111100001101000011110000111100001111000011110000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[5]~input_o\,
	datab => \ALT_INV_D[7]~input_o\,
	datac => \ALT_INV_D[6]~input_o\,
	datad => \ALT_INV_D[3]~input_o\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst2|G1:7:OC:BC2|FA|o_carry~0_combout\);

-- Location: IOIBUF_X89_Y36_N21
\D[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(0),
	o => \D[0]~input_o\);

-- Location: IOIBUF_X89_Y37_N4
\D[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(1),
	o => \D[1]~input_o\);

-- Location: IOIBUF_X89_Y37_N38
\D[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D(2),
	o => \D[2]~input_o\);

-- Location: LABCELL_X88_Y36_N42
\inst2|G1:3:OC:BC2|FA|o_carry\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:3:OC:BC2|FA|o_carry~combout\ = ( \D[1]~input_o\ & ( \D[2]~input_o\ & ( (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\) # ((!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ & ((\D[3]~input_o\))) # (\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ & (\D[0]~input_o\ & 
-- !\D[3]~input_o\))) ) ) ) # ( !\D[1]~input_o\ & ( \D[2]~input_o\ & ( (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\) # ((\D[0]~input_o\ & (!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ & \D[3]~input_o\))) ) ) ) # ( \D[1]~input_o\ & ( !\D[2]~input_o\ & ( 
-- (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & (((!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\) # (!\D[3]~input_o\)) # (\D[0]~input_o\))) ) ) ) # ( !\D[1]~input_o\ & ( !\D[2]~input_o\ & ( (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & 
-- ((!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ & ((\D[3]~input_o\) # (\D[0]~input_o\))) # (\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ & ((!\D[3]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000011000000111100001101000011110000111101001111000111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[0]~input_o\,
	datab => \inst1|G1:4:OC:BC2|FA|ALT_INV_o_carry~0_combout\,
	datac => \inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\,
	datad => \ALT_INV_D[3]~input_o\,
	datae => \ALT_INV_D[1]~input_o\,
	dataf => \ALT_INV_D[2]~input_o\,
	combout => \inst2|G1:3:OC:BC2|FA|o_carry~combout\);

-- Location: MLABCELL_X87_Y36_N0
\inst2|G1:7:OC:BC2|FA|o_carry\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:7:OC:BC2|FA|o_carry~combout\ = ( \inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( ((!\inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\ & \inst1|G1:2:OC:BC2|O~0_combout\)) # (\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\) ) 
-- ) ) # ( !\inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( \inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ ) ) ) # ( \inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( !\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( 
-- (!\inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\) # ((\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\) # (\inst1|G1:2:OC:BC2|O~0_combout\)) ) ) ) # ( !\inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( !\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( 
-- (\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\) # (\inst1|G1:2:OC:BC2|O~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111101111111011111100001111000011110010111100101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|G1:4:OC:BC2|FA|ALT_INV_w_WIRE_1~combout\,
	datab => \inst1|G1:2:OC:BC2|ALT_INV_O~0_combout\,
	datac => \inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~0_combout\,
	datae => \inst2|G1:3:OC:BC2|FA|ALT_INV_o_carry~combout\,
	dataf => \inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\,
	combout => \inst2|G1:7:OC:BC2|FA|o_carry~combout\);

-- Location: LABCELL_X88_Y36_N54
\inst2|G1:2:OC:BC2|FA|o_carry\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:2:OC:BC2|FA|o_carry~combout\ = ( \D[2]~input_o\ & ( (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\) # ((!\D[0]~input_o\ & (!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ & \D[1]~input_o\)) # (\D[0]~input_o\ & ((!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\) # 
-- (\D[1]~input_o\)))) ) ) # ( !\D[2]~input_o\ & ( (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ((!\D[0]~input_o\ & (!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\ & \D[1]~input_o\)) # (\D[0]~input_o\ & ((!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\) # 
-- (\D[1]~input_o\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000011010000010000001101000011110100111111011111010011111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[0]~input_o\,
	datab => \inst1|G1:4:OC:BC2|FA|ALT_INV_o_carry~0_combout\,
	datac => \inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\,
	datad => \ALT_INV_D[1]~input_o\,
	dataf => \ALT_INV_D[2]~input_o\,
	combout => \inst2|G1:2:OC:BC2|FA|o_carry~combout\);

-- Location: LABCELL_X88_Y36_N0
\inst2|G1:7:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:7:OC:BC2|O~0_combout\ = ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( (!\D[6]~input_o\ & (\D[7]~input_o\ & ((\D[3]~input_o\) # (\D[5]~input_o\)))) ) ) ) # ( !\inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( 
-- (\D[5]~input_o\ & (!\D[6]~input_o\ & \D[7]~input_o\)) ) ) ) # ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( !\D[4]~input_o\ & ( (\D[5]~input_o\ & (!\D[6]~input_o\ & \D[7]~input_o\)) ) ) ) # ( !\inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( !\D[4]~input_o\ & ( 
-- (\D[5]~input_o\ & (!\D[6]~input_o\ & \D[7]~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000010100000000000001110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[5]~input_o\,
	datab => \ALT_INV_D[3]~input_o\,
	datac => \ALT_INV_D[6]~input_o\,
	datad => \ALT_INV_D[7]~input_o\,
	datae => \inst2|G1:2:OC:BC2|FA|ALT_INV_o_carry~combout\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst2|G1:7:OC:BC2|O~0_combout\);

-- Location: LABCELL_X88_Y36_N36
\inst2|G1:6:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:6:OC:BC2|O~0_combout\ = ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( (!\D[6]~input_o\ & (!\D[5]~input_o\ & (!\D[3]~input_o\ & \D[7]~input_o\))) # (\D[6]~input_o\ & (!\D[7]~input_o\ & ((\D[3]~input_o\) # (\D[5]~input_o\)))) ) ) 
-- ) # ( !\inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( (!\D[5]~input_o\ & (!\D[6]~input_o\ & \D[7]~input_o\)) # (\D[5]~input_o\ & (\D[6]~input_o\ & !\D[7]~input_o\)) ) ) ) # ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( !\D[4]~input_o\ & ( 
-- (!\D[5]~input_o\ & (!\D[6]~input_o\ & \D[7]~input_o\)) # (\D[5]~input_o\ & (\D[6]~input_o\ & !\D[7]~input_o\)) ) ) ) # ( !\inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( !\D[4]~input_o\ & ( (!\D[5]~input_o\ & (!\D[6]~input_o\ & \D[7]~input_o\)) # 
-- (\D[5]~input_o\ & (\D[6]~input_o\ & !\D[7]~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110100000000001011010000000000101101000000000011110000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[5]~input_o\,
	datab => \ALT_INV_D[3]~input_o\,
	datac => \ALT_INV_D[6]~input_o\,
	datad => \ALT_INV_D[7]~input_o\,
	datae => \inst2|G1:2:OC:BC2|FA|ALT_INV_o_carry~combout\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst2|G1:6:OC:BC2|O~0_combout\);

-- Location: MLABCELL_X87_Y36_N9
\inst2|G1:5:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:5:OC:BC2|O~0_combout\ = ( \inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( (!\inst1|G1:2:OC:BC2|O~0_combout\ & (\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ & !\inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\)) # 
-- (\inst1|G1:2:OC:BC2|O~0_combout\ & ((\inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\))) ) ) ) # ( !\inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( \inst1|G1:2:OC:BC2|O~0_combout\ ) ) ) # ( 
-- \inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( !\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( (!\inst1|G1:2:OC:BC2|O~0_combout\ & (\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ & \inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\)) # (\inst1|G1:2:OC:BC2|O~0_combout\ & 
-- ((!\inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\))) ) ) ) # ( !\inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( !\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( (\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ & !\inst1|G1:2:OC:BC2|O~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000000011110101000000001111000011110101000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~0_combout\,
	datac => \inst1|G1:2:OC:BC2|ALT_INV_O~0_combout\,
	datad => \inst2|G1:4:OC:BC2|FA|ALT_INV_w_WIRE_1~combout\,
	datae => \inst2|G1:3:OC:BC2|FA|ALT_INV_o_carry~combout\,
	dataf => \inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\,
	combout => \inst2|G1:5:OC:BC2|O~0_combout\);

-- Location: LABCELL_X88_Y36_N12
\inst2|G1:4:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:4:OC:BC2|O~0_combout\ = ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( (!\D[5]~input_o\ & ((!\D[3]~input_o\ & ((\D[7]~input_o\) # (\D[6]~input_o\))) # (\D[3]~input_o\ & (\D[6]~input_o\ & \D[7]~input_o\)))) # (\D[5]~input_o\ & 
-- (!\D[3]~input_o\)) ) ) ) # ( !\inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( (((\D[7]~input_o\) # (\D[6]~input_o\)) # (\D[3]~input_o\)) # (\D[5]~input_o\) ) ) ) # ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( !\D[4]~input_o\ & ( 
-- (\D[3]~input_o\ & ((!\D[6]~input_o\ & ((\D[7]~input_o\) # (\D[5]~input_o\))) # (\D[6]~input_o\ & ((!\D[7]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000100110011000001111111111111110100110011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[5]~input_o\,
	datab => \ALT_INV_D[3]~input_o\,
	datac => \ALT_INV_D[6]~input_o\,
	datad => \ALT_INV_D[7]~input_o\,
	datae => \inst2|G1:2:OC:BC2|FA|ALT_INV_o_carry~combout\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst2|G1:4:OC:BC2|O~0_combout\);

-- Location: LABCELL_X88_Y36_N18
\inst2|G1:3:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:3:OC:BC2|O~0_combout\ = ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( (!\D[3]~input_o\ & (((!\D[6]~input_o\) # (!\D[7]~input_o\)))) # (\D[3]~input_o\ & (!\D[5]~input_o\ & (\D[6]~input_o\ & \D[7]~input_o\))) ) ) ) # ( 
-- !\inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( \D[4]~input_o\ & ( !\D[3]~input_o\ $ ((((\D[7]~input_o\) # (\D[6]~input_o\)) # (\D[5]~input_o\))) ) ) ) # ( \inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( !\D[4]~input_o\ & ( (!\D[6]~input_o\ & (!\D[3]~input_o\ & 
-- ((\D[7]~input_o\) # (\D[5]~input_o\)))) # (\D[6]~input_o\ & ((!\D[3]~input_o\ $ (\D[7]~input_o\)))) ) ) ) # ( !\inst2|G1:2:OC:BC2|FA|o_carry~combout\ & ( !\D[4]~input_o\ & ( (\D[3]~input_o\ & (((\D[7]~input_o\) # (\D[6]~input_o\)) # (\D[5]~input_o\))) ) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001001100110011010011001100001110010011001100111100110011000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[5]~input_o\,
	datab => \ALT_INV_D[3]~input_o\,
	datac => \ALT_INV_D[6]~input_o\,
	datad => \ALT_INV_D[7]~input_o\,
	datae => \inst2|G1:2:OC:BC2|FA|ALT_INV_o_carry~combout\,
	dataf => \ALT_INV_D[4]~input_o\,
	combout => \inst2|G1:3:OC:BC2|O~0_combout\);

-- Location: LABCELL_X88_Y36_N24
\inst2|G1:2:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:2:OC:BC2|O~0_combout\ = ( \D[1]~input_o\ & ( \D[2]~input_o\ & ( (!\inst2|G1:7:OC:BC2|FA|o_carry~combout\) # (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ $ (((!\D[0]~input_o\ & \inst1|G1:4:OC:BC2|FA|o_carry~0_combout\)))) ) ) ) # ( !\D[1]~input_o\ & 
-- ( \D[2]~input_o\ & ( (!\inst2|G1:7:OC:BC2|FA|o_carry~combout\) # (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ $ (((!\D[0]~input_o\) # (\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\)))) ) ) ) # ( \D[1]~input_o\ & ( !\D[2]~input_o\ & ( 
-- (\inst2|G1:7:OC:BC2|FA|o_carry~combout\ & (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ $ (((!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\) # (\D[0]~input_o\))))) ) ) ) # ( !\D[1]~input_o\ & ( !\D[2]~input_o\ & ( (\inst2|G1:7:OC:BC2|FA|o_carry~combout\ & 
-- (!\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ $ (((\D[0]~input_o\ & !\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000100110000000000110010000111011110110011111111110011011110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[0]~input_o\,
	datab => \inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~combout\,
	datac => \inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\,
	datad => \inst1|G1:4:OC:BC2|FA|ALT_INV_o_carry~0_combout\,
	datae => \ALT_INV_D[1]~input_o\,
	dataf => \ALT_INV_D[2]~input_o\,
	combout => \inst2|G1:2:OC:BC2|O~0_combout\);

-- Location: LABCELL_X88_Y36_N57
\inst2|G1:1:OC:BC2|O~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:1:OC:BC2|O~0_combout\ = !\D[1]~input_o\ $ (((!\inst2|G1:7:OC:BC2|FA|o_carry~combout\) # (!\D[0]~input_o\ $ (!\inst1|G1:4:OC:BC2|FA|o_carry~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100111110110000010011111011000001001111101100000100111110110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_D[0]~input_o\,
	datab => \inst1|G1:4:OC:BC2|FA|ALT_INV_o_carry~0_combout\,
	datac => \inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~combout\,
	datad => \ALT_INV_D[1]~input_o\,
	combout => \inst2|G1:1:OC:BC2|O~0_combout\);

-- Location: MLABCELL_X87_Y36_N12
\inst2|G1:0:FC:BC1|O\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|G1:0:FC:BC1|O~combout\ = ( \inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( !\D[0]~input_o\ $ (((!\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ & ((!\inst1|G1:2:OC:BC2|O~0_combout\) # 
-- (\inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\))))) ) ) ) # ( !\inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( \inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( !\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\ $ (!\D[0]~input_o\) ) ) ) # ( \inst2|G1:3:OC:BC2|FA|o_carry~combout\ & 
-- ( !\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( !\D[0]~input_o\ $ (((\inst2|G1:4:OC:BC2|FA|w_WIRE_1~combout\ & (!\inst1|G1:2:OC:BC2|O~0_combout\ & !\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\)))) ) ) ) # ( !\inst2|G1:3:OC:BC2|FA|o_carry~combout\ & ( 
-- !\inst|G1:1:OC:BC2|FA|w_WIRE_2~combout\ & ( !\D[0]~input_o\ $ (((!\inst1|G1:2:OC:BC2|O~0_combout\ & !\inst2|G1:7:OC:BC2|FA|o_carry~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111111000000101111110100000000001111111100000010111111010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst2|G1:4:OC:BC2|FA|ALT_INV_w_WIRE_1~combout\,
	datab => \inst1|G1:2:OC:BC2|ALT_INV_O~0_combout\,
	datac => \inst2|G1:7:OC:BC2|FA|ALT_INV_o_carry~0_combout\,
	datad => \ALT_INV_D[0]~input_o\,
	datae => \inst2|G1:3:OC:BC2|FA|ALT_INV_o_carry~combout\,
	dataf => \inst|G1:1:OC:BC2|FA|ALT_INV_w_WIRE_2~combout\,
	combout => \inst2|G1:0:FC:BC1|O~combout\);

-- Location: LABCELL_X62_Y16_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


