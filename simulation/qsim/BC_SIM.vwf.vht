-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "01/15/2020 18:31:07"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          BCell
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY BCell_vhd_vec_tst IS
END BCell_vhd_vec_tst;
ARCHITECTURE BCell_arch OF BCell_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL A : STD_LOGIC;
SIGNAL B : STD_LOGIC;
SIGNAL Ci : STD_LOGIC;
SIGNAL Cout : STD_LOGIC;
SIGNAL Output : STD_LOGIC;
SIGNAL Select : STD_LOGIC;
SIGNAL SelectO : STD_LOGIC;
COMPONENT BCell
	PORT (
	A : IN STD_LOGIC;
	B : IN STD_LOGIC;
	Ci : IN STD_LOGIC;
	Cout : OUT STD_LOGIC;
	Output : OUT STD_LOGIC;
	\Select\ : IN STD_LOGIC;
	SelectO : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : BCell
	PORT MAP (
-- list connections between master ports and signals
	A => A,
	B => B,
	Ci => Ci,
	Cout => Cout,
	Output => Output,
	\Select\ => Select,
	SelectO => SelectO
	);

-- A
t_prcs_A: PROCESS
BEGIN
LOOP
	A <= '0';
	WAIT FOR 50000 ps;
	A <= '1';
	WAIT FOR 50000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_A;

-- B
t_prcs_B: PROCESS
BEGIN
LOOP
	B <= '0';
	WAIT FOR 100000 ps;
	B <= '1';
	WAIT FOR 100000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_B;

-- Ci
t_prcs_Ci: PROCESS
BEGIN
	FOR i IN 1 TO 2
	LOOP
		Ci <= '0';
		WAIT FOR 200000 ps;
		Ci <= '1';
		WAIT FOR 200000 ps;
	END LOOP;
	Ci <= '0';
WAIT;
END PROCESS t_prcs_Ci;

-- Select
t_prcs_Select: PROCESS
BEGIN
	Select <= '0';
	WAIT FOR 400000 ps;
	Select <= '1';
	WAIT FOR 400000 ps;
	Select <= '0';
WAIT;
END PROCESS t_prcs_Select;
END BCell_arch;
