library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity BCS is
	port(
	
	A, MuxI				: in std_logic;
	
	O, Cout, MuxO		: out std_logic
	
	);
end BCS;

architecture RTL of BCS is

	begin
		
		Cout <= A;
		MuxO <= MuxI;
		O 	  <= A xor MuxI;

end RTL;