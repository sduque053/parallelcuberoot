library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lvl2 is
	port(
	
		D : in std_logic_vector(4 downto 0);
		
		P : in std_logic_vector(4 downto 1);
		
		R : out std_logic_vector(4 downto 0);
		
		Q : out std_logic
		
		);
end lvl2;

architecture RTL of lvl2 is

	component BC
		port(
	
			A, B, Cin, MuxI	: in std_logic;
	
			O, Cout, MuxO		: out std_logic
	
		);
	end component;
	
	component BCS
		port(
	
			A, MuxI				: in std_logic;
	
			O, Cout, MuxO		: out std_logic
	
		);
	end component;
	
	signal CO : std_logic_vector(4 downto 0);
	signal CI : std_logic_vector(4 downto 1);
	signal MO : std_logic_vector(4 downto 0);
	signal MI : std_logic_vector(4 downto 0);
	
	
	begin
	
		G1: for I in 0 to 4 generate
			
			FC: if I=0 generate
				BC1: BCS port map(
					D(I), MI(I), R(I), CO(I), MO(I)
				);
			end generate FC;
			
			OC: if i>0 generate
				BC2: BC port map(
					D(I), P(I), CI(I), MI(I), R(I), CO(I), MO(I)
				);
			end generate OC;
			
		end generate G1;
		
		MI(4) <= CO(4);
		Q <= MO(0);
		
		G2: for I in 0 to 3 generate
			CI(I+1) <= CO(I);
			MI(I) <= MO(I+1);
		end generate G2;
		

end RTL;
		