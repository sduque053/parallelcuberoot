library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BitSize32 is
	port(
	
		data		: in std_logic_vector(31 downto 0);
		remaind	: out std_logic_vector(31 downto 0);
		root		: out std_logic_vector(10 downto 0)
	
	);
end BitSize32;

architecture RTL of BitSize32 is

	signal remaindv: std_logic_vector(186 downto 0);
	signal factor	: std_logic_vector(137 downto 0);
	signal PartRoot: std_logic_vector(10 downto 0);

	begin

		C1: 	entity work.lvl1 			port map(data(31 downto 30)													  , remaindv(1 downto 0), PartRoot(10));
		
		CC1:	entity work.CircComb1 	port map(PartRoot(10)		   ,factor(3 downto 0));
		
		C2: 	entity work.lvl2 			port map(remaindv(1 downto 0)&data(29 downto 27), factor(3 downto 0), remaindv(6 downto 2), PartRoot(9));
		
		CC2:	entity work.CircComb2	port map(PartRoot(10 downto 9), factor(9 downto 4));
		
		C3: 	entity work.lvl3 			port map(remaindv(6 downto 2)&data(26 downto 24), factor(9 downto 4), remaindv(14 downto 7), PartRoot(8));
		
		CC3:	entity work.CircComb3	port map(PartRoot(10 downto 8), factor(18 downto 10));
		
		C4: 	entity work.lvl4 			port map(remaindv(14 downto 7)&data(23 downto 21), factor(18 downto 10), remaindv(25 downto 15), PartRoot(7));
		
		CC4:	entity work.CircComb4	port map(PartRoot(10 downto 7), factor(29 downto 19));
		
		C5:	entity work.lvl5			port map(remaindv(25 downto 15)&data(20 downto 18), factor(29 downto 19), remaindv(39 downto 26), PartRoot(6));
		
		CC5: 	entity work.CircComb5	port map(PartRoot(10 downto 6), factor(42 downto 30));
		
		C6:	entity work.lvl6			port map(remaindv(39 downto 26)&data(17 downto 15), factor(42 downto 30), remaindv(56 downto 40), PartRoot(5));
		
		CC6:	entity work.CircComb6	port map(PartRoot(10 downto 5), factor(57 downto 43));
		
		C7:	entity work.lvl7			port map(remaindv(56 downto 40)&data(14 downto 12), factor(57 downto 43), remaindv(76 downto 57), PartRoot(4));
		
		CC7:	entity work.CircComb7	port map(PartRoot(10 downto 4), factor(74 downto 58));
		
		C8: 	entity work.lvl8 			port map(remaindv(76 downto 57)&data(11 downto 9), factor(74 downto 58), remaindv(99 downto 77), PartRoot(3));
		
		CC8: 	entity work.CircComb8	port map(PartRoot(10 downto 3), factor(93 downto 75));
		
		C9:	entity work.lvl9			port map(remaindv(99 downto 77)&data(8 downto 6), factor(93 downto 75), remaindv(125 downto 100), PartRoot(2));
		
		CC9:	entity work.CircComb9	port map(PartRoot(10 downto 2), factor(114 downto 94));
		
		C10:	entity work.lvl10			port map(remaindv(125 downto 100)&data(5 downto 3), factor(114 downto 94), remaindv(154 downto 126), PartRoot(1));
		
		CC10:	entity work.CircComb10	port map(PartRoot(10 downto 1), factor(137 downto 115));
		
		C11: 	entity work.lvl11			port map(remaindv(154 downto 126)&data(2 downto 0), factor(137 downto 115), remaindv(186 downto 155), PartRoot(0));
		
		
		remaind(31 downto 0) <= remaindv(186 downto 155);
		root <= PartRoot;
		
end RTL;
	