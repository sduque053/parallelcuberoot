library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BitSize16 is
	port(
	
		data		: in std_logic_vector(15 downto 0);
		remaind	: out std_logic_vector(15 downto 0);
		root		: out std_logic_vector(5 downto 0)
	
	);
end BitSize16;

architecture RTL of BitSize16 is

	signal remaindv: std_logic_vector(56 downto 0);
	signal factor	: std_logic_vector(42 downto 0);
	signal PartRoot: std_logic_vector(5 downto 0);

	begin

		C1: 	entity work.lvl1 			port map('0'&data(15), remaindv(1 downto 0), PartRoot(5));
		CC1:	entity work.CircComb1 	port map(PartRoot(5),factor(3 downto 0));
		C2: 	entity work.lvl2 			port map(remaindv(1 downto 0)&data(14 downto 12), factor(3 downto 0), remaindv(6 downto 2), PartRoot(4));
		CC2:	entity work.CircComb2	port map(PartRoot(5 downto 4), factor(9 downto 4));
		C3: 	entity work.lvl3 			port map(remaindv(6 downto 2)&data(11 downto 9), factor(9 downto 4), remaindv(14 downto 7), PartRoot(3));
		CC3:	entity work.CircComb3	port map(PartRoot(5 downto 3), factor(18 downto 10));
		C4: 	entity work.lvl4 			port map(remaindv(14 downto 7)&data(8 downto 6), factor(18 downto 10), remaindv(25 downto 15), PartRoot(2));
		CC4:	entity work.CircComb4	port map(PartRoot(5 downto 2), factor(29 downto 19));
		C5:	entity work.lvl5			port map(remaindv(25 downto 15)&data(5 downto 3), factor(29 downto 19), remaindv(39 downto 26), PartRoot(1));
		CC5: 	entity work.CircComb5	port map(PartRoot(5 downto 1), factor(42 downto 30));
		C6:	entity work.lvl6			port map(remaindv(39 downto 26)&data(2 downto 0), factor(42 downto 30), remaindv(56 downto 40), PartRoot(0));
		
		
		remaind(15 downto 0) <= remaindv(55 downto 40);
		root <= PartRoot;
		
end RTL;
	