library ieee;
use ieee.std_logic_1164.all;
 
entity CircComb3 is
  port (
    q  : in std_logic_vector(2 downto 0);
    --
    F : out std_logic_vector(38 downto 30)
    );
end CircComb3;
 
 
architecture rtl of CircComb3 is


begin

	with q select F <=
	
	"000000000" when "000",
		
	"000001001" when "001",
	
	"000011110" when "010",
	
	"000111111" when "011",
	
	"001101100" when "100",
		
	"010100101" when "101",
	
	"011101010" when "110",
	
	"100111011" when "111";

 
end rtl;