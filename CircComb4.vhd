library ieee;
use ieee.std_logic_1164.all;
 
entity CircComb4 is
  port (
    q  : in std_logic_vector(3 downto 0);
    --
    F : out std_logic_vector(410 downto 400)
    );
end CircComb4;
 
 
architecture rtl of CircComb4 is


begin

	with q select F <=
	
	"00000000000" when "0000",
	"00000001001" when "0001",
	"00000011110" when "0010",
	"00000111111" when "0011",
	"00001101100" when "0100",
	"00010100101" when "0101",
	"00011101010" when "0110",
	"00100111011" when "0111",
	"00110011000" when "1000",
	"01000000001" when "1001",
	"01001110110" when "1010",
	"01011110111" when "1011",
	"01110000100" when "1100",
	"10000011101" when "1101",
	"10011000010" when "1110",
	"10101110011" when "1111";
 
end rtl;