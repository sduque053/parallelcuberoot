library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity BC is
	port(
	
	A, B, Cin, MuxI	: in std_logic;
	
	O, Cout, MuxO		: out std_logic
	
	);
end BC;

architecture RTL of BC is

	signal Os  : std_logic;
	signal Bfa : std_logic;
	
	begin
		
		Bfa <= not B;

		FA: entity work.full_adder port map(A, Bfa, Cin, Os, Cout);	
		
		MuxO <= MuxI;
		
		with MuxI select O <=
			A  when '0',
			Os when '1';
			
		
end RTL;