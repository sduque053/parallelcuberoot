library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BitSize8 is
	port(
	
		data		: in std_logic_vector(7 downto 0);
		remaind	: out std_logic_vector(7 downto 0);
		root		: out std_logic_vector(2 downto 0)
	
	);
end BitSize8;

architecture RTL of BitSize8 is

	signal remaindv: std_logic_vector(6 downto 0);
	signal factor	: std_logic_vector(9 downto 0);
	signal PartRoot: std_logic_vector(2 downto 0);

	begin

		C1: 	entity work.lvl1 			port map(data(7 downto 6), remaindv(1 downto 0), PartRoot(2));
		CC1:	entity work.CircComb1 	port map(PartRoot(2),factor(3 downto 0));
		C2: 	entity work.lvl2 			port map(remaindv(1 downto 0)&data(5 downto 3), factor(3 downto 0), remaindv(6 downto 2), PartRoot(1));
		CC2:	entity work.CircComb2	port map(PartRoot(2 downto 1), factor(9 downto 4));
		C3: 	entity work.lvl3 			port map(remaindv(6 downto 2)&data(2 downto 0), factor(9 downto 4), remaind(7 downto 0), PartRoot(0));
		
		root <= PartRoot;
		
end RTL;
	