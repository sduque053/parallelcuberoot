library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PllCrct is
	port(
	
		D : in std_logic_vector(1 downto 0);
		
		P : in std_logic_vector(1 downto 0);
		
		R : out std_logic_vector(1 downto 0);
		
		Q : out std_logic
		
		);
end PllCrct;

architecture RTL of PllCrct is

	component BC
		port(
	
			A, B, Cin, MuxI	: in std_logic;
	
			O, Cout, MuxO		: out std_logic
	
		);
	end component;
	
	signal CO : std_logic_vector(1 downto 0);
	signal CI : std_logic_vector(1 downto 0);
	signal MO : std_logic_vector(1 downto 0);
	signal MI : std_logic_vector(1 downto 0);
	
	
	begin
	
		G1: for I in 0 to 1 generate
			cell: BC port map(
				D(I), P(I), CI(I), MI(I), R(I), CO(I), MO(I)
				);
		
		end generate G1;
		
		CI(0) <= '1';
		MI(1) <= CO(1);
		Q <= MO(0);
		
		G2: for I in 0 to 0 generate
			CI(I+1) <= CO(I);
			MI(I) <= MO(I+1);
		end generate;
		
end RTL;
		